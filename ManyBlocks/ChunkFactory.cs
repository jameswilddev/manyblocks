﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Called by <see cref="IChunkCache{TBlock}"/> when it needs to add a chunk to the cache.</summary>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    public interface IChunkFactory<TBlock>
    {
        /// <summary>Gets a chunk to cache.  This loads from persistent storage, or generates the chunk if new.</summary>
        /// <remarks><see cref="IChunkCache{TBlock}"/> handles concurrency; this should not be called multiple times for the same <see cref="ChunkLocation"/>.</remarks>
        /// <param name="location">The <see cref="ChunkLocation"/> to retrieve.</param>
        /// <returns>The chunk to add to the cache.</returns>
        Task<TBlock[,,]> Get(ChunkLocation location);
    }

    /// <inheritdoc />
    [Export(typeof(IChunkFactory<>))]
    public sealed class ChunkFactory<TBlock> : IChunkFactory<TBlock> where TBlock : struct
    {
        /// <inheritdoc />
        [Import]
        public IChunkPersistence<TBlock> ChunkPersistence;

        /// <inheritdoc />
        [Import]
        public IChunkGenerator<TBlock> ChunkGenerator;

        /// <inheritdoc />
        public async Task<TBlock[,,]> Get(ChunkLocation location)
        {
            var chunk = await ChunkPersistence.TryLoad(location);
            if (chunk != null) return chunk;

            chunk = await ChunkGenerator.Generate(location);
            await ChunkPersistence.Create(location, chunk);
            return chunk;
        }
    }
}
