﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>A point of view in the world.</summary>
    public struct PointOfView
    {
        /// <summary>The location of the beholder.</summary>
        public Vector3 Location;

        /// <summary>The direction in which the beholder is looking.</summary>
        public PitchYaw Angle;
    }
}
