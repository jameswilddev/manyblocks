﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Raise this event to change a block.</summary>
    /// <remarks>Note that no authorization check will be done.</remarks>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    public struct BlockChange<TBlock> where TBlock : struct
    {
        /// <summary>When non-<see langword="null"/>, the id of the player who caused the change.</summary>
        public Guid? PlayerId;

        /// <summary>The <see cref="GlobalBlockLocation"/> at which a block has changed.</summary>
        public GlobalBlockLocation Location;

        /// <summary>The <typeparamref name="TBlock"/> the block changed to.</summary>
        public TBlock Block;
    }

    /// <summary>Raised when a chunk changes.</summary>
    /// <remarks>This is automatically raised by <see cref="IChunkCache{TBlock}"/> when it adds chunks to the cache and when it applies <see cref="BlockChange{TBlock}"/>s.</remarks>
    public struct ChunkChange
    {
        /// <summary>The <see cref="ChunkLocation"/> which has changed.</summary>
        public ChunkLocation Location;
    }
}
