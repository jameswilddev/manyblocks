﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>A global aggregator for events.  Used to propagate state changes between components.</summary>
    /// <typeparam name="T">A type encapsulating the event's metadata.</typeparam>
    public interface IEventSink<T>
    {
        /// <summary>The wrapped event.  Call <see cref="Raise"/> to raise.</summary>
        event Action<T> Event;

        /// <summary>Raises <see cref="Event"/>.</summary>
        /// <param name="metadata">The metadata for the event.</param>
        void Raise(T metadata);
    }

    /// <inheritdoc />
    [Export(typeof(IEventSink<>))]
    public sealed class EventSink<T> : IEventSink<T>
    {
        /// <inheritdoc />
        public event Action<T> Event;

        /// <inheritdoc />
        public void Raise(T metadata)
        {
            var @event = Event;
            if (@event != null)
                @event(metadata);
        }
    }
}
