﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Export with <see cref="IProfilable"/> to have your component be able to report profiling statistics.</summary>
    public interface IProfilableMetadata
    {
        /// <summary>The name of the component being profiled.</summary>
        string Name { get; }
    }

    /// <summary>Export with <see cref="IProfilableMetadata"/> to have your component be able to report profiling statistics.</summary>
    public interface IProfilable
    {
        /// <summary>Gets one or more statistics describing the component's performance characteristics.</summary>
        IEnumerable<KeyValuePair<string, string>> Statistics { get; }
    }
}
