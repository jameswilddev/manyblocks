﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Sits in front of <see cref="IChunkCache{TBlock}"/>, allowing chunks to be accessed block-by-block.</summary>
    /// <remarks>This is a separate interface rather than a set of extension methods to make read-only block access easier to mock.</remarks>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    public interface IBlockCache<TBlock> where TBlock : struct
    {
        /// <summary>If the chunk containing a <typeparamref name="TBlock"/> is in the cache, retrieves it.</summary>
        /// <param name="location">The <see cref="GlobalBlockLocation"/> to retrieve from.</param>
        /// <returns>If the chunk containing <paramref name="location"/> has been loaded, the specified <typeparamref name="TBlock"/>, else, <see langword="null"/>.</returns>
        TBlock? TryGet(GlobalBlockLocation location);

        /// <summary>Retrieves a <typeparamref name="TBlock"/>.  If the parent chunk is not in the cache, it is loaded from persistent storage or generated.</summary>
        /// <remarks>This could be computationally expensive, so use sparingly.</remarks>
        /// <param name="location">The <see cref="GlobalBlockLocation"/> to retrieve from.</param>
        /// <returns>The <typeparamref name="TBlock"/> at <paramref name="location"/>.</returns>
        Task<TBlock> Get(GlobalBlockLocation location);
    }

    /// <inheritdoc />
    [Export(typeof(IBlockCache<>))]
    public sealed class BlockCache<TBlock> : IBlockCache<TBlock> where TBlock : struct
    {
        /// <inheritdoc />
        [Import]
        public IChunkCache<TBlock> ChunkCache;

        /// <inheritdoc />
        public async Task<TBlock> Get(GlobalBlockLocation location)
        {
            var block = location.Block;
            return (await ChunkCache.Get(location.Chunk))[block.X, block.Y, block.Z];
        }

        /// <inheritdoc />
        public TBlock? TryGet(GlobalBlockLocation location)
        {
            var result = ChunkCache.TryGet(location.Chunk);
            if (result == null) return null;
            var block = location.Block;
            return result[block.X, block.Y, block.Z];
        }
    }
}
