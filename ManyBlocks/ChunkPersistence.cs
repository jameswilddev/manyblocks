﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Provides persistent storage for chunks.</summary>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    public interface IChunkPersistence<TBlock> where TBlock : struct
    {
        /// <summary>Attempt to load a chunk.</summary>
        /// <remarks>Will never be called multiple times at the same time for the same <paramref name="location"/>, or during a call to <see cref="Create"/> or <see cref="SaveChange"/> for the same <paramref name="location"/>.</remarks>
        /// <param name="location">The <see cref="ChunkLocation"/> to attempt to retrieve.</param>
        /// <returns>If no chunk has ever been <see cref="Create"/>d for <paramref name="location"/>, <see langword="null"/>, else, the saved chunk.</returns>
        Task<TBlock[,,]> TryLoad(ChunkLocation location);

        /// <summary>Creates a new chunk.</summary>
        /// <remarks>Will never be called twice for the same <paramref name="location"/>, or at the same time as <see cref="TryLoad"/> or <see cref="SaveChange"/> for the same <paramref name="location"/>.</remarks>
        /// <param name="location">The <see cref="ChunkLocation"/> to save.</param>
        /// <param name="chunk">The chunk to save.</param>
        Task Create(ChunkLocation location, TBlock[,,] chunk);

        /// <summary>Saves a change to an existing chunk.</summary>
        /// <remarks>Will never be called for chunks never <see cref="Create"/>d or successfully <see cref="TryLoad"/>ed.  Calls will be queued up per <paramref name="chunk"/> and executed one at a time.</remarks>
        /// <param name="playerId">The id of the player which caused the change, when non-<see langword="null"/>.</param>
        /// <param name="chunk">The <see cref="ChunkLocation"/> to save a change for.</param>
        /// <param name="block">The <see cref="LocalBlockLocation"/> to save a change for.</param>
        /// <param name="to">The <typeparamref name="TBlock"/> changed to.</param>
        Task SaveChange(Guid? playerId, ChunkLocation chunk, LocalBlockLocation block, TBlock to);
    }
}
