﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Container for a pitch/yaw pair.  Most commonly used to retain where an entity is looking.</summary>
    public struct PitchYaw
    {
        /// <summary>Zero is looking at the horizon.  <see cref="Math.PI"/>/2 is looking straight up.  -<see cref="Math.PI"/>/2 is looking straight down.</summary>
        public float Pitch;

        /// <summary>Zero is looking forward along Z+.  <see cref="Math.PI"/>/2 is looking right along X+.</summary>
        public float Yaw;
    }
}
