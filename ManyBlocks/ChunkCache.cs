﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>A container for all in-memory chunks.</summary>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    public interface IChunkCache<TBlock>
    {
        /// <summary>If a chunk is in the cache, retrieves it.</summary>
        /// <param name="location">The <see cref="ChunkLocation"/> to retrieve from.</param>
        /// <returns>If a chunk has been cached at <paramref name="location"/>, its blocks, else, <see langword="null"/>.</returns>
        TBlock[,,] TryGet(ChunkLocation location);

        /// <summary>Retrieves a chunk, loading it into the cache if not already present and generating it if not already existing in persistent storage.</summary>
        /// <remarks>This could be computationally expensive, so use sparingly.  Raises the <see cref="ChunkChange"/> event on adding a new chunk to the cache.</remarks>
        /// <param name="location">The <see cref="ChunkLocation"/> to retrieve from.</param>
        /// <returns>The chunk at <paramref name="location"/>.</returns>
        Task<TBlock[,,]> Get(ChunkLocation location);
    }

    /// <inheritdoc />
    [Export(typeof(IChunkCache<>))]
    [ExportMetadata("Name", "Chunk Cache")]
    public sealed class ChunkCache<TBlock> : IChunkCache<TBlock>, IProfilable
    {
        /// <inheritdoc />
        [Import]
        public IChunkFactory<TBlock> ChunkFactory;

        /// <inheritdoc />
        [Import]
        public IEventSink<ChunkChange> ChunkChange;

        private readonly ConcurrentDictionary<ChunkLocation, Task<TBlock[,,]>> Cached = new ConcurrentDictionary<ChunkLocation, Task<TBlock[,,]>>();
        private readonly ConcurrentDictionary<ChunkLocation, TBlock[,,]> CachedResults = new ConcurrentDictionary<ChunkLocation, TBlock[,,]>();

        private async Task<TBlock[,,]> GetAndCache(ChunkLocation location)
        {
            var result = await ChunkFactory.Get(location);
            CachedResults[location] = result;
            ChunkChange.Raise(new ChunkChange { Location = location });
            return result;
        }

        /// <inheritdoc />
        public async Task<TBlock[,,]> Get(ChunkLocation location)
        {
            return await Cached.GetOrAdd(location, GetAndCache);
        }

        /// <inheritdoc />
        public TBlock[,,] TryGet(ChunkLocation location)
        {
            TBlock[,,] output;
            CachedResults.TryGetValue(location, out output);
            return output;
        }

        /// <inheritdoc />
        public IEnumerable<KeyValuePair<string, string>> Statistics
        {
            get
            {
                yield return new KeyValuePair<string, string>("Chunks loading", (Cached.Count - CachedResults.Count).ToString());
                yield return new KeyValuePair<string, string>("Chunks in memory", (CachedResults.Count).ToString());
            }
        }
    }
}
