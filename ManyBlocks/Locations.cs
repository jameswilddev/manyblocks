﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>The location of a block in world space.</summary>
    public struct GlobalBlockLocation
    {
        /// <summary>Negative values to the left, positive values to the right.</summary>
        public int X;

        /// <summary>Negative values below, positive values above.</summary>
        public int Y;

        /// <summary>Negative values behind, positive values in front.</summary>
        public int Z;

        /// <summary>Each chunk is this many blocks along each axis.</summary>
        public const int ChunkSize = 32;

        private short ChunkAxis(int input)
        {
            if (input >= 0)
                return (short)(input / ChunkSize);

            return (short)((input - ChunkSize + 1) / ChunkSize);
        }

        /// <summary>Gets the containing <see cref="ChunkLocation"/>.</summary>
        public ChunkLocation Chunk
        {
            get
            {
                return new ChunkLocation
                {
                    X = ChunkAxis(X),
                    Y = ChunkAxis(Y),
                    Z = ChunkAxis(Z)
                };
            }
        }

        private byte BlockAxis(int input)
        {
            return (byte)(input - ChunkAxis(input) * ChunkSize);
        }

        /// <summary>Gets the <see cref="LocalBlockLocation"/> within the containing <see cref="ChunkLocation"/>.</summary>
        public LocalBlockLocation Block
        {
            get
            {
                return new LocalBlockLocation
                {
                    X = BlockAxis(X),
                    Y = BlockAxis(Y),
                    Z = BlockAxis(Z)
                };
            }
        }

        /// <summary>Gets the above <see cref="GlobalBlockLocation"/>.</summary>
        public GlobalBlockLocation Above { get { return new GlobalBlockLocation { X = X, Y = Y + 1, Z = Z }; } }

        /// <summary>Gets the below <see cref="GlobalBlockLocation"/>.</summary>
        public GlobalBlockLocation Below { get { return new GlobalBlockLocation { X = X, Y = Y - 1, Z = Z }; } }

        /// <inheritdoc />
        /// <param name="chunk">The containing <see cref="ChunkLocation"/>.</param>
        /// <param name="block">The <see cref="LocalBlockLocation"/> within <paramref name="chunk"/>.</param>
        public GlobalBlockLocation(ChunkLocation chunk, LocalBlockLocation block)
        {
            X = chunk.X * ChunkSize + block.X;
            Y = chunk.Y * ChunkSize + block.Y;
            Z = chunk.Z * ChunkSize + block.Z;
        }

        /// <inheritdoc />
        /// <param name="location">The <see cref="Vector3"/> to get the containing <see cref="GlobalBlockLocation"/> of.</param>
        public GlobalBlockLocation(Vector3 location)
        {
            X = (int)Math.Floor(location.X);
            Y = (int)Math.Floor(location.Y);
            Z = (int)Math.Floor(location.Z);
        }
    }

    /// <summary>The location of a block relative to the left back lower corner of its parent chunk.</summary>
    public struct LocalBlockLocation
    {
        /// <summary>Negative values to the left, positive values to the right.</summary>
        public byte X;

        /// <summary>Negative values below, positive values above.</summary>
        public byte Y;

        /// <summary>Negative values behind, positive values in front.</summary>
        public byte Z;
    }

    /// <summary>The location of a chunk in world space.</summary>
    public struct ChunkLocation
    {
        /// <summary>Negative values to the left, positive values to the right.</summary>
        public short X;

        /// <summary>Negative values below, positive values above.</summary>
        public short Y;

        /// <summary>Negative values behind, positive values in front.</summary>
        public short Z;
    }
}
