﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Lists <see cref="ChunkLocation"/>s relevant to a specified <see cref="PointOfView"/>.</summary>
    public interface INearbyChunks
    {
        /// <summary>Lists <see cref="ChunkLocation"/>s relevant to a specified <see cref="PointOfView"/>.</summary>
        /// <remarks>This is used to determine which chunks to load.</remarks>
        /// <param name="pointOfView">The <see cref="PointOfView"/> to find <see cref="ChunkLocation"/>s relevant to.</param>
        /// <returns>The <see cref="ChunkLocation"/>s relevant to <paramref name="pointOfView"/>.</returns>
        IEnumerable<ChunkLocation> NearTo(PointOfView pointOfView);
    }

    /// <inheritdoc />
    [Export(typeof(INearbyChunks))]
    public sealed class NearbyChunks : INearbyChunks
    {
        /// <inheritdoc />
        [Import]
        public ServerSettings ServerSettings;

        private static IEnumerable<ChunkLocation> InfiniteNearTo(ChunkLocation location)
        {
            yield return location;

            // Create progressively larger cubic shells around the starting location.
            for (var size = 1; ; size++)
            {
                var left = (short)(location.X - size);
                var right = (short)(location.X + size);
                var bottom = (short)(location.Y - size);
                var top = (short)(location.Y + size);
                var back = (short)(location.Z - size);
                var front = (short)(location.Z + size);

                // Bottom and top layers.
                for (var x = left; x <= right; x++)
                    for (var z = back; z <= front; z++)
                    {
                        yield return new ChunkLocation { X = x, Y = bottom, Z = z };
                        yield return new ChunkLocation { X = x, Y = top, Z = z };
                    }

                // Fill the sandwich.
                for (var y = (short)(bottom + 1); y < top; y++)
                {
                    // Left and right sides, including the front/back corners.
                    for (var z = back; z <= front; z++)
                    {
                        yield return new ChunkLocation { X = left, Y = y, Z = z };
                        yield return new ChunkLocation { X = right, Y = y, Z = z };
                    }

                    // Front and back sides, not including the left/right corners.
                    for (var x = (short)(left + 1); x < right; x++)
                    {
                        yield return new ChunkLocation { X = x, Y = y, Z = back };
                        yield return new ChunkLocation { X = x, Y = y, Z = front };
                    }
                }
            }
        }

        /// <inheritdoc />
        public IEnumerable<ChunkLocation> NearTo(PointOfView pointOfView)
        {
            // In future, this algorithm may bias in the direction the player is looking.  At present it does not.

            return InfiniteNearTo(new GlobalBlockLocation(pointOfView.Location).Chunk).Take(ServerSettings.VisibleChunks);
        }
    }
}
