﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Container for settings required to run a server.</summary>
    /// <remarks>Create an instance yourself and inject it into the <see cref="CompositionContainer"/>.</remarks>
    public sealed class ServerSettings
    {
        /// <summary>The id of the world in play.  Used by <see cref="IChunkPersistence{TBlock}"/>.</summary>
        public Guid WorldId;

        /// <summary>Used by <see cref="IChunkGenerator{TBlock}"/>.</summary>
        public Guid Seed;

        /// <summary>The number of chunks which can be seen by players.</summary>
        public int VisibleChunks;
    }
}
