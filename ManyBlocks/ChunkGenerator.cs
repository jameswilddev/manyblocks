﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks
{
    /// <summary>Generates a new chunk based on a seed and <see cref="ChunkLocation"/>.</summary>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    public interface IChunkGenerator<TBlock> where TBlock : struct
    {
        /// <summary>Generates a new chunk based on <see cref="ServerSettings.Seed"/> and a <see cref="ChunkLocation"/>.</summary>
        /// <param name="location">The <see cref="ChunkLocation"/> to generate for.</param>
        /// <returns>The generated chunk.</returns>
        Task<TBlock[,,]> Generate(ChunkLocation location);
    }
}
