﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Rendering
{
    /// <summary>The location of a pixel inside a texture.</summary>
    public struct TextureCoordinate
    {
        /// <summary>Zero is the left border of the texture.</summary>
        public ushort X;

        /// <summary>Zero is the top border of the texture.</summary>
        public ushort Y;
    }

    /// <summary>Generates a texture atlas combining many equally sized, square, smaller textures into a single image.</summary>
    /// <typeparam name="TFace">The face type in use.  Each gets its own tile in the atlas.</typeparam>
    /// <typeparam name="TColor">The color type in use.</typeparam>
    public interface IFaceAtlas<TFace, TColor> where TFace : struct where TColor : struct
    {
        /// <summary>The combined texture atlas.  <see cref="Width"/>x<see cref="Height"/>.  Runs from the top left, to the right, then down.</summary>
        TColor[] Atlas { get; }

        /// <summary>The width of the texture atlas in pixels.  Guaranteed to be a power of two.</summary>
        int Width { get; }

        /// <summary>The height of the texture atlas in pixels.  Guaranteed to be a power of two.</summary>
        int Height { get; }

        /// <summary>The width/height in pixels of each face.</summary>
        int Size { get; }

        /// <summary>The locations of the top left corners of each <typeparamref name="TFace"/>.</summary>
        ReadOnlyDictionary<TFace, TextureCoordinate> Faces { get; }

        /// <summary>Creates a new atlas from a given set of <typeparamref name="TFace"/>s.</summary>
        /// <param name="size"><see cref="Size"/>.</param>
        /// <param name="faces">The faces to load.  Runs from the top left, to the right, then down.  Each must be <paramref name="size"/>x<paramref name="size"/>.</param>
        void Load(int size, Dictionary<TFace, TColor[]> faces);
    }

    /// <inheritdoc />
    [Export(typeof(IFaceAtlas<,>))]
    public sealed class FaceAtlas<TFace, TColor> : IFaceAtlas<TFace, TColor> where TFace : struct where TColor : struct
    {
        /// <inheritdoc />
        public TColor[] Atlas { get; private set; }

        /// <inheritdoc />
        public ReadOnlyDictionary<TFace, TextureCoordinate> Faces { get; private set; }

        /// <inheritdoc />
        public int Width { get; private set; }

        /// <inheritdoc />
        public int Height { get; private set; }

        /// <inheritdoc />
        public int Size { get; private set; }

        /// <inheritdoc />
        public void Load(int size, Dictionary<TFace, TColor[]> faces)
        {
            Size = size;
            Width = 1;
            Height = 1;
            while (true)
            {
                var packX = size;
                var packY = size;

                var done = true;

                for (var face = 0; face < faces.Count; face++)
                {
                    if (packX > Width)
                    {
                        packX = size;
                        packY += size;
                        if (packY > Height)
                        {
                            if (Width > Height)
                                Height *= 2;
                            else
                                Width *= 2;
                            done = false;
                            break;
                        }
                    }
                    else
                        packX += size;
                }

                while (Height < size) Height *= 2;

                if (!done) continue;

                packX = packY = 0;
                Atlas = new TColor[Width * Height];

                var outFaces = new Dictionary<TFace, TextureCoordinate>();
                Faces = new ReadOnlyDictionary<TFace, TextureCoordinate>(outFaces);
                foreach (var face in faces)
                {
                    outFaces[face.Key] = new TextureCoordinate { X = (ushort)packX, Y = (ushort)packY };
                    for (var x = 0; x < size; x++)
                        for (var y = 0; y < size; y++)
                            Atlas[(packX + x) + (packY + y) * Width] = face.Value[x + y * size];
                    packX += size;
                    if((packX + size) > Width)
                    {
                        packX = 0;
                        packY += size;
                    }
                }

                return;
            }
        }
    }
}
