﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Rendering
{
    /// <summary>Provides the ability to render the world.</summary>
    /// <remarks>Most implementations require this be used only during <see cref="IGame.Render"/>.</remarks>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    /// <typeparam name="TFace">The face type in use.</typeparam>
    /// <typeparam name="TColor">The color type in use.</typeparam>
    public interface IRenderer<TBlock, TFace, TColor> where TBlock : struct where TFace : struct where TColor : struct
    {
        /// <summary>Renders the scene.</summary>
        /// <remarks>Behavior may be undefined should the render area extend beyond the viewport bounds.</remarks>
        /// <param name="left">The distance in pixels from the left border of the viewport to the left border of the render area.</param>
        /// <param name="bottom">The distance in pixels from the bottom border of the viewport to the bottom border of the render area.</param>
        /// <param name="width">The width of the viewport in pixels.</param>
        /// <param name="height">The height of the viewport in pixels.</param>
        /// <param name="eye">The location of the camera.</param>
        /// <param name="look">The normalized <see cref="Vector3"/> along which the camera is looking.</param>
        /// <param name="up">The normalized <see cref="Vector3"/> specifying which way up the camera is.</param>
        /// <param name="fieldOfView">The field of view, in radians.</param>
        void Render(int left, int bottom, int width, int height, Vector3 eye, Vector3 look, Vector3 up, float fieldOfView);
    }
}
