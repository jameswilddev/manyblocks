﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Rendering
{
    /// <summary>A vertex in a mesh.</summary>
    public struct Vertex
    {
        /// <summary>The location of the vertex.</summary>
        public GlobalBlockLocation Location;

        /// <summary>The texture coordinate, in pixels, where zero is the top left border.</summary>
        public TextureCoordinate TextureCoordinate;
    }

    /// <summary>A collection of <see cref="Vertex"/>es describing a quadrilateral, clockwise.</summary>
    public struct Face
    {
        /// <summary>The bottom-left vertex of the face.</summary>
        public Vertex A;

        /// <summary>The top-left vertex of the face.</summary>
        public Vertex B;

        /// <summary>The top-right vertex of the face.</summary>
        public Vertex C;

        /// <summary>The bottom-right vertex of the face.</summary>
        public Vertex D;
    }

    /// <summary>Converts given chunks into streams of <see cref="Faces"/> for rendering.</summary>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    /// <typeparam name="TFace">The face type in use.</typeparam>
    /// <typeparam name="TColor">The color type in use.</typeparam>
    public interface IChunkMeshConverter<TBlock, TFace, TColor> where TBlock : struct where TFace : struct where TColor : struct
    {
        /// <summary>Generates a sequence of mesh vertices for a given chunk.</summary>
        /// <param name="location">The <see cref="ChunkLocation"/> of <paramref name="chunk"/>.</param>
        /// <param name="chunk">The chunk to generate a mesh for.</param>
        /// <returns>A sequence of faces, describing a mesh to represent <paramref name="chunk"/>.</returns>
        IEnumerable<Face> Faces(ChunkLocation location, TBlock[,,] chunk);

        /// <summary>Generates a sequence of mesh indices for a given number of faces.</summary>
        /// <param name="faces">The number of faces to generate indices for.</param>
        /// <returns>A sequence of mesh indices for the number of <paramref name="faces"/> specified.  Each triplet represents a triangle drawn clockwise.</returns>
        IEnumerable<ushort> Indices(int faces);
    }

    /// <inheritdoc />
    [Export(typeof(IChunkMeshConverter<,,>))]
    public sealed class ChunkMeshConverter<TBlock, TFace, TColor> : IChunkMeshConverter<TBlock, TFace, TColor> where TBlock : struct where TFace : struct where TColor : struct
    {
        /// <inheritdoc />
        [Import]
        public IBlockFaceConverter<TBlock, TFace> BlockFaceConverter;

        /// <inheritdoc />
        [Import]
        public IFaceAtlas<TFace, TColor> FaceAtlas;

        private Face MakeFace(TFace face, GlobalBlockLocation a, GlobalBlockLocation b, GlobalBlockLocation c, GlobalBlockLocation d)
        {
            var coord = FaceAtlas.Faces[face];
            var coordX = (ushort)(coord.X + FaceAtlas.Size);
            var coordY = (ushort)(coord.Y + FaceAtlas.Size);

            // Putting the positive Y axis at the bottom of the face may look odd, but in image editors zero is at the top.
            return new Face
            {
                // Bottom left.
                A = new Vertex
                {
                    Location = a,
                    TextureCoordinate = new TextureCoordinate { X = coord.X, Y = coordY }
                },

                // Top left.
                B = new Vertex
                {
                    Location = b,
                    TextureCoordinate = coord
                },

                // Top right.
                C = new Vertex
                {
                    Location = c,
                    TextureCoordinate = new TextureCoordinate { X = coordX, Y = coord.Y }
                },

                // Bottom right.
                D = new Vertex
                {
                    Location = d,
                    TextureCoordinate = new TextureCoordinate { X = coordX, Y = coordY }
                }
            };
        }

        /// <inheritdoc />
        public IEnumerable<Face> Faces(ChunkLocation location, TBlock[,,] chunk)
        {
            for (byte x = 0; x < GlobalBlockLocation.ChunkSize; x++)
                for (byte y = 0; y < GlobalBlockLocation.ChunkSize; y++)
                    for (byte z = 0; z < GlobalBlockLocation.ChunkSize; z++)
                    {
                        var block = new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = y, Z = z });
                        TFace? xN, xP, yN, yP, zN, zP;
                        BlockFaceConverter.Convert(chunk[x, y, z], block, out xN, out xP, out yN, out yP, out zN, out zP);

                        if (xN.HasValue)
                            yield return MakeFace
                            (
                                xN.Value,
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = y, Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = (byte)(y + 1), Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = (byte)(y + 1), Z = z }),
                                block
                            );

                        if (xP.HasValue)
                            yield return MakeFace
                            (
                                xP.Value,
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = y, Z = z }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = (byte)(y + 1), Z = z }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = (byte)(y + 1), Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = y, Z = (byte)(z + 1) })
                            );

                        if (yN.HasValue)
                            yield return MakeFace
                            (
                                yN.Value,
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = y, Z = z }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = y, Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = y, Z = (byte)(z + 1) }),
                                block
                            );

                        if (yP.HasValue)
                            yield return MakeFace
                            (
                                yP.Value,
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = (byte)(y + 1), Z = z }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = (byte)(y + 1), Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = (byte)(y + 1), Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = (byte)(y + 1), Z = z })
                            );

                        if (zN.HasValue)
                            yield return MakeFace
                            (
                                zN.Value,
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = y, Z = z }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = (byte)(y + 1), Z = z }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = (byte)(y + 1), Z = z }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = y, Z = z })
                            );

                        if (zP.HasValue)
                            yield return MakeFace
                            (
                                zP.Value,
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = y, Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = (byte)(x + 1), Y = (byte)(y + 1), Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = (byte)(y + 1), Z = (byte)(z + 1) }),
                                new GlobalBlockLocation(location, new LocalBlockLocation { X = x, Y = y, Z = (byte)(z + 1) })
                            );
                    }
        }

        /// <inheritdoc />
        public IEnumerable<ushort> Indices(int faces)
        {
            for (var face = 0; face < faces; face++)
            {
                yield return (ushort)(face * 4);
                yield return (ushort)(face * 4 + 1);
                yield return (ushort)(face * 4 + 2);
                yield return (ushort)(face * 4 + 2);
                yield return (ushort)(face * 4 + 3);
                yield return (ushort)(face * 4);
            }
        }
    }
}
