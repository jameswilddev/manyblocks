﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Rendering
{
    /// <summary>Container for global game logic.</summary>
    public interface IGame
    {
        /// <summary>Called once per frame by <see cref="IPlatform"/> to draw the scene.</summary>
        /// <param name="width">The width of the viewport; this will be the window or screen on which the game is being displayed.</param>
        /// <param name="height">The height of the viewport; this will be the window or screen on which the game is being displayed.</param>
        void Render(int width, int height);
    }
}
