﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Rendering
{
    /// <summary>Instantiated at startup.  Should create shared resources and then call <see cref="IGame.Render"/>.</summary>
    public interface IPlatform
    {
    }
}
