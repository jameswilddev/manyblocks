﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Rendering
{
    /// <summary>Determines, given a block, which faces are visible and what is shown on them.</summary>
    /// <typeparam name="TBlock">The block type in use.</typeparam>
    /// <typeparam name="TFace">The face type in use.</typeparam>
    public interface IBlockFaceConverter<TBlock, TFace> where TBlock : struct where TFace : struct
    {
        /// <summary>Determines which faces to show for a given block.</summary>
        /// <param name="block">The <typeparamref name="TBlock"/> in question.</param>
        /// <param name="location">The <see cref="GlobalBlockLocation"/> of the block in question.</param>
        /// <param name="xN">When non-<see langword="null"/>, the <typeparamref name="TFace"/> to show on the left side.</param>
        /// <param name="xP">When non-<see langword="null"/>, the <typeparamref name="TFace"/> to show on the right side.</param>
        /// <param name="yN">When non-<see langword="null"/>, the <typeparamref name="TFace"/> to show on the bottom.</param>
        /// <param name="yP">When non-<see langword="null"/>, the <typeparamref name="TFace"/> to show on top.</param>
        /// <param name="zN">When non-<see langword="null"/>, the <typeparamref name="TFace"/> to show on the back.</param>
        /// <param name="zP">When non-<see langword="null"/>, the <typeparamref name="TFace"/> to show on the front.</param>
        void Convert(TBlock block, GlobalBlockLocation location, out TFace? xN, out TFace? xP, out TFace? yN, out TFace? yP, out TFace? zN, out TFace? zP);
    }
}
