﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ManyBlocks.Rendering.Unit
{
    [TestClass]
    public sealed class FaceAtlasTests
    {
        public enum DummyColor
        {
            Face_A_Pixel_AA,
            Face_A_Pixel_AB,
            Face_A_Pixel_AC,
            Face_A_Pixel_BA,
            Face_A_Pixel_BB,
            Face_A_Pixel_BC,
            Face_A_Pixel_CA,
            Face_A_Pixel_CB,
            Face_A_Pixel_CC,

            Face_B_Pixel_AA,
            Face_B_Pixel_AB,
            Face_B_Pixel_AC,
            Face_B_Pixel_BA,
            Face_B_Pixel_BB,
            Face_B_Pixel_BC,
            Face_B_Pixel_CA,
            Face_B_Pixel_CB,
            Face_B_Pixel_CC,

            Face_C_Pixel_AA,
            Face_C_Pixel_AB,
            Face_C_Pixel_AC,
            Face_C_Pixel_BA,
            Face_C_Pixel_BB,
            Face_C_Pixel_BC,
            Face_C_Pixel_CA,
            Face_C_Pixel_CB,
            Face_C_Pixel_CC
        }

        public enum DummyFace
        {
            Face_A,
            Face_B,
            Face_C
        }

        [TestMethod]
        public void OneNonPowerOfTwoTiles()
        {
            var inputTiles = new Dictionary<DummyFace, DummyColor[]>
            {
                {
                    DummyFace.Face_A,
                    new[]
                    {
                        DummyColor.Face_A_Pixel_AA, DummyColor.Face_A_Pixel_AB, DummyColor.Face_A_Pixel_AC,
                        DummyColor.Face_A_Pixel_BA, DummyColor.Face_A_Pixel_BB, DummyColor.Face_A_Pixel_BC,
                        DummyColor.Face_A_Pixel_CA, DummyColor.Face_A_Pixel_CB, DummyColor.Face_A_Pixel_CC
                    }
                }
            };

            var instance = new FaceAtlas<DummyFace, DummyColor>();
            instance.Load(3, inputTiles);

            Assert.AreEqual(3, instance.Size);
            Assert.AreEqual(0, instance.Width % 2);
            Assert.AreEqual(0, instance.Height % 2);
            Assert.IsTrue(instance.Width == instance.Height || instance.Width == instance.Height * 2 || instance.Width * 2 == instance.Height);
            Assert.AreEqual(instance.Width * instance.Height, instance.Atlas.Length);

            Func<DummyFace, int, int, DummyColor> getColor = (face, x, y) =>
            {
                x += instance.Faces[face].X;
                y += instance.Faces[face].Y;
                Assert.IsTrue(x >= 0);
                Assert.IsTrue(y >= 0);
                Assert.IsTrue(x < instance.Width);
                Assert.IsTrue(y < instance.Height);
                return instance.Atlas[y * instance.Width + x];
            };

            Assert.AreEqual(DummyColor.Face_A_Pixel_AA, getColor(DummyFace.Face_A, 0, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AB, getColor(DummyFace.Face_A, 1, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AC, getColor(DummyFace.Face_A, 2, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BA, getColor(DummyFace.Face_A, 0, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BB, getColor(DummyFace.Face_A, 1, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BC, getColor(DummyFace.Face_A, 2, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CA, getColor(DummyFace.Face_A, 0, 2));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CB, getColor(DummyFace.Face_A, 1, 2));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CC, getColor(DummyFace.Face_A, 2, 2));
        }

        [TestMethod]
        public void TwoNonPowerOfTwoTiles()
        {
            var inputTiles = new Dictionary<DummyFace, DummyColor[]>
            {
                {
                    DummyFace.Face_A,
                    new[]
                    {
                        DummyColor.Face_A_Pixel_AA, DummyColor.Face_A_Pixel_AB, DummyColor.Face_A_Pixel_AC,
                        DummyColor.Face_A_Pixel_BA, DummyColor.Face_A_Pixel_BB, DummyColor.Face_A_Pixel_BC,
                        DummyColor.Face_A_Pixel_CA, DummyColor.Face_A_Pixel_CB, DummyColor.Face_A_Pixel_CC
                    }
                },
                {
                    DummyFace.Face_B,
                    new[]
                    {
                        DummyColor.Face_B_Pixel_AA, DummyColor.Face_B_Pixel_AB, DummyColor.Face_B_Pixel_AC,
                        DummyColor.Face_B_Pixel_BA, DummyColor.Face_B_Pixel_BB, DummyColor.Face_B_Pixel_BC,
                        DummyColor.Face_B_Pixel_CA, DummyColor.Face_B_Pixel_CB, DummyColor.Face_B_Pixel_CC
                    }
                }
            };

            var instance = new FaceAtlas<DummyFace, DummyColor>();
            instance.Load(3, inputTiles);

            Assert.AreEqual(3, instance.Size);
            Assert.AreEqual(0, instance.Width % 2);
            Assert.AreEqual(0, instance.Height % 2);
            Assert.IsTrue(instance.Width == instance.Height || instance.Width == instance.Height * 2 || instance.Width * 2 == instance.Height);
            Assert.AreEqual(instance.Width * instance.Height, instance.Atlas.Length);

            Func<DummyFace, int, int, DummyColor> getColor = (face, x, y) =>
            {
                x += instance.Faces[face].X;
                y += instance.Faces[face].Y;
                Assert.IsTrue(x >= 0);
                Assert.IsTrue(y >= 0);
                Assert.IsTrue(x < instance.Width);
                Assert.IsTrue(y < instance.Height);
                return instance.Atlas[y * instance.Width + x];
            };

            Assert.AreEqual(DummyColor.Face_A_Pixel_AA, getColor(DummyFace.Face_A, 0, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AB, getColor(DummyFace.Face_A, 1, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AC, getColor(DummyFace.Face_A, 2, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BA, getColor(DummyFace.Face_A, 0, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BB, getColor(DummyFace.Face_A, 1, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BC, getColor(DummyFace.Face_A, 2, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CA, getColor(DummyFace.Face_A, 0, 2));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CB, getColor(DummyFace.Face_A, 1, 2));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CC, getColor(DummyFace.Face_A, 2, 2));

            Assert.AreEqual(DummyColor.Face_B_Pixel_AA, getColor(DummyFace.Face_B, 0, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_AB, getColor(DummyFace.Face_B, 1, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_AC, getColor(DummyFace.Face_B, 2, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BA, getColor(DummyFace.Face_B, 0, 1));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BB, getColor(DummyFace.Face_B, 1, 1));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BC, getColor(DummyFace.Face_B, 2, 1));
            Assert.AreEqual(DummyColor.Face_B_Pixel_CA, getColor(DummyFace.Face_B, 0, 2));
            Assert.AreEqual(DummyColor.Face_B_Pixel_CB, getColor(DummyFace.Face_B, 1, 2));
            Assert.AreEqual(DummyColor.Face_B_Pixel_CC, getColor(DummyFace.Face_B, 2, 2));
        }

        [TestMethod]
        public void ThreeNonPowerOfTwoTiles()
        {
            var inputTiles = new Dictionary<DummyFace, DummyColor[]>
            {
                {
                    DummyFace.Face_A,
                    new[]
                    {
                        DummyColor.Face_A_Pixel_AA, DummyColor.Face_A_Pixel_AB, DummyColor.Face_A_Pixel_AC,
                        DummyColor.Face_A_Pixel_BA, DummyColor.Face_A_Pixel_BB, DummyColor.Face_A_Pixel_BC,
                        DummyColor.Face_A_Pixel_CA, DummyColor.Face_A_Pixel_CB, DummyColor.Face_A_Pixel_CC
                    }
                },
                {
                    DummyFace.Face_B,
                    new[]
                    {
                        DummyColor.Face_B_Pixel_AA, DummyColor.Face_B_Pixel_AB, DummyColor.Face_B_Pixel_AC,
                        DummyColor.Face_B_Pixel_BA, DummyColor.Face_B_Pixel_BB, DummyColor.Face_B_Pixel_BC,
                        DummyColor.Face_B_Pixel_CA, DummyColor.Face_B_Pixel_CB, DummyColor.Face_B_Pixel_CC
                    }
                },
                {
                    DummyFace.Face_C,
                    new[]
                    {
                        DummyColor.Face_C_Pixel_AA, DummyColor.Face_C_Pixel_AB, DummyColor.Face_C_Pixel_AC,
                        DummyColor.Face_C_Pixel_BA, DummyColor.Face_C_Pixel_BB, DummyColor.Face_C_Pixel_BC,
                        DummyColor.Face_C_Pixel_CA, DummyColor.Face_C_Pixel_CB, DummyColor.Face_C_Pixel_CC
                    }
                }
            };

            var instance = new FaceAtlas<DummyFace, DummyColor>();
            instance.Load(3, inputTiles);

            Assert.AreEqual(3, instance.Size);
            Assert.AreEqual(0, instance.Width % 2);
            Assert.AreEqual(0, instance.Height % 2);
            Assert.IsTrue(instance.Width == instance.Height || instance.Width == instance.Height * 2 || instance.Width * 2 == instance.Height);
            Assert.AreEqual(instance.Width * instance.Height, instance.Atlas.Length);

            Func<DummyFace, int, int, DummyColor> getColor = (face, x, y) =>
            {
                x += instance.Faces[face].X;
                y += instance.Faces[face].Y;
                Assert.IsTrue(x >= 0);
                Assert.IsTrue(y >= 0);
                Assert.IsTrue(x < instance.Width);
                Assert.IsTrue(y < instance.Height);
                return instance.Atlas[y * instance.Width + x];
            };

            Assert.AreEqual(DummyColor.Face_A_Pixel_AA, getColor(DummyFace.Face_A, 0, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AB, getColor(DummyFace.Face_A, 1, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AC, getColor(DummyFace.Face_A, 2, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BA, getColor(DummyFace.Face_A, 0, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BB, getColor(DummyFace.Face_A, 1, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BC, getColor(DummyFace.Face_A, 2, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CA, getColor(DummyFace.Face_A, 0, 2));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CB, getColor(DummyFace.Face_A, 1, 2));
            Assert.AreEqual(DummyColor.Face_A_Pixel_CC, getColor(DummyFace.Face_A, 2, 2));

            Assert.AreEqual(DummyColor.Face_B_Pixel_AA, getColor(DummyFace.Face_B, 0, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_AB, getColor(DummyFace.Face_B, 1, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_AC, getColor(DummyFace.Face_B, 2, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BA, getColor(DummyFace.Face_B, 0, 1));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BB, getColor(DummyFace.Face_B, 1, 1));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BC, getColor(DummyFace.Face_B, 2, 1));
            Assert.AreEqual(DummyColor.Face_B_Pixel_CA, getColor(DummyFace.Face_B, 0, 2));
            Assert.AreEqual(DummyColor.Face_B_Pixel_CB, getColor(DummyFace.Face_B, 1, 2));
            Assert.AreEqual(DummyColor.Face_B_Pixel_CC, getColor(DummyFace.Face_B, 2, 2));

            Assert.AreEqual(DummyColor.Face_C_Pixel_AA, getColor(DummyFace.Face_C, 0, 0));
            Assert.AreEqual(DummyColor.Face_C_Pixel_AB, getColor(DummyFace.Face_C, 1, 0));
            Assert.AreEqual(DummyColor.Face_C_Pixel_AC, getColor(DummyFace.Face_C, 2, 0));
            Assert.AreEqual(DummyColor.Face_C_Pixel_BA, getColor(DummyFace.Face_C, 0, 1));
            Assert.AreEqual(DummyColor.Face_C_Pixel_BB, getColor(DummyFace.Face_C, 1, 1));
            Assert.AreEqual(DummyColor.Face_C_Pixel_BC, getColor(DummyFace.Face_C, 2, 1));
            Assert.AreEqual(DummyColor.Face_C_Pixel_CA, getColor(DummyFace.Face_C, 0, 2));
            Assert.AreEqual(DummyColor.Face_C_Pixel_CB, getColor(DummyFace.Face_C, 1, 2));
            Assert.AreEqual(DummyColor.Face_C_Pixel_CC, getColor(DummyFace.Face_C, 2, 2));
        }

        [TestMethod]
        public void OnePowerOfTwoTile()
        {
            var inputTiles = new Dictionary<DummyFace, DummyColor[]>
            {
                {
                    DummyFace.Face_A,
                    new[]
                    {
                        DummyColor.Face_A_Pixel_AA, DummyColor.Face_A_Pixel_AB,
                        DummyColor.Face_A_Pixel_BA, DummyColor.Face_A_Pixel_BB
                    }
                }
            };

            var instance = new FaceAtlas<DummyFace, DummyColor>();
            instance.Load(2, inputTiles);

            Assert.AreEqual(2, instance.Size);
            Assert.AreEqual(0, instance.Width % 2);
            Assert.AreEqual(0, instance.Height % 2);
            Assert.IsTrue(instance.Width == instance.Height || instance.Width == instance.Height * 2 || instance.Width * 2 == instance.Height);
            Assert.AreEqual(instance.Width * instance.Height, instance.Atlas.Length);

            Func<DummyFace, int, int, DummyColor> getColor = (face, x, y) =>
            {
                x += instance.Faces[face].X;
                y += instance.Faces[face].Y;
                Assert.IsTrue(x >= 0);
                Assert.IsTrue(y >= 0);
                Assert.IsTrue(x < instance.Width);
                Assert.IsTrue(y < instance.Height);
                return instance.Atlas[y * instance.Width + x];
            };

            Assert.AreEqual(DummyColor.Face_A_Pixel_AA, getColor(DummyFace.Face_A, 0, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AB, getColor(DummyFace.Face_A, 1, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BA, getColor(DummyFace.Face_A, 0, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BB, getColor(DummyFace.Face_A, 1, 1));
        }

        [TestMethod]
        public void TwoPowerOfTwoTiles()
        {
            var inputTiles = new Dictionary<DummyFace, DummyColor[]>
            {
                {
                    DummyFace.Face_A,
                    new[]
                    {
                        DummyColor.Face_A_Pixel_AA, DummyColor.Face_A_Pixel_AB,
                        DummyColor.Face_A_Pixel_BA, DummyColor.Face_A_Pixel_BB
                    }
                },
                {
                    DummyFace.Face_B,
                    new[]
                    {
                        DummyColor.Face_B_Pixel_AA, DummyColor.Face_B_Pixel_AB,
                        DummyColor.Face_B_Pixel_BA, DummyColor.Face_B_Pixel_BB
                    }
                }
            };

            var instance = new FaceAtlas<DummyFace, DummyColor>();
            instance.Load(2, inputTiles);

            Assert.AreEqual(2, instance.Size);
            Assert.AreEqual(0, instance.Width % 2);
            Assert.AreEqual(0, instance.Height % 2);
            Assert.IsTrue(instance.Width == instance.Height || instance.Width == instance.Height * 2 || instance.Width * 2 == instance.Height);
            Assert.AreEqual(instance.Width * instance.Height, instance.Atlas.Length);

            Func<DummyFace, int, int, DummyColor> getColor = (face, x, y) =>
            {
                x += instance.Faces[face].X;
                y += instance.Faces[face].Y;
                Assert.IsTrue(x >= 0);
                Assert.IsTrue(y >= 0);
                Assert.IsTrue(x < instance.Width);
                Assert.IsTrue(y < instance.Height);
                return instance.Atlas[y * instance.Width + x];
            };

            Assert.AreEqual(DummyColor.Face_A_Pixel_AA, getColor(DummyFace.Face_A, 0, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AB, getColor(DummyFace.Face_A, 1, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BA, getColor(DummyFace.Face_A, 0, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BB, getColor(DummyFace.Face_A, 1, 1));

            Assert.AreEqual(DummyColor.Face_B_Pixel_AA, getColor(DummyFace.Face_B, 0, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_AB, getColor(DummyFace.Face_B, 1, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BA, getColor(DummyFace.Face_B, 0, 1));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BB, getColor(DummyFace.Face_B, 1, 1));
        }

        [TestMethod]
        public void ThreePowerOfTwoTiles()
        {
            var inputTiles = new Dictionary<DummyFace, DummyColor[]>
            {
                {
                    DummyFace.Face_A,
                    new[]
                    {
                        DummyColor.Face_A_Pixel_AA, DummyColor.Face_A_Pixel_AB,
                        DummyColor.Face_A_Pixel_BA, DummyColor.Face_A_Pixel_BB
                    }
                },
                {
                    DummyFace.Face_B,
                    new[]
                    {
                        DummyColor.Face_B_Pixel_AA, DummyColor.Face_B_Pixel_AB,
                        DummyColor.Face_B_Pixel_BA, DummyColor.Face_B_Pixel_BB
                    }
                },
                {
                    DummyFace.Face_C,
                    new[]
                    {
                        DummyColor.Face_C_Pixel_AA, DummyColor.Face_C_Pixel_AB,
                        DummyColor.Face_C_Pixel_BA, DummyColor.Face_C_Pixel_BB,
                    }
                }
            };

            var instance = new FaceAtlas<DummyFace, DummyColor>();
            instance.Load(2, inputTiles);

            Assert.AreEqual(2, instance.Size);
            Assert.AreEqual(0, instance.Width % 2);
            Assert.AreEqual(0, instance.Height % 2);
            Assert.IsTrue(instance.Width == instance.Height || instance.Width == instance.Height * 2 || instance.Width * 2 == instance.Height);
            Assert.AreEqual(instance.Width * instance.Height, instance.Atlas.Length);

            Func<DummyFace, int, int, DummyColor> getColor = (face, x, y) =>
            {
                x += instance.Faces[face].X;
                y += instance.Faces[face].Y;
                Assert.IsTrue(x >= 0);
                Assert.IsTrue(y >= 0);
                Assert.IsTrue(x < instance.Width);
                Assert.IsTrue(y < instance.Height);
                return instance.Atlas[y * instance.Width + x];
            };

            Assert.AreEqual(DummyColor.Face_A_Pixel_AA, getColor(DummyFace.Face_A, 0, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_AB, getColor(DummyFace.Face_A, 1, 0));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BA, getColor(DummyFace.Face_A, 0, 1));
            Assert.AreEqual(DummyColor.Face_A_Pixel_BB, getColor(DummyFace.Face_A, 1, 1));

            Assert.AreEqual(DummyColor.Face_B_Pixel_AA, getColor(DummyFace.Face_B, 0, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_AB, getColor(DummyFace.Face_B, 1, 0));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BA, getColor(DummyFace.Face_B, 0, 1));
            Assert.AreEqual(DummyColor.Face_B_Pixel_BB, getColor(DummyFace.Face_B, 1, 1));

            Assert.AreEqual(DummyColor.Face_C_Pixel_AA, getColor(DummyFace.Face_C, 0, 0));
            Assert.AreEqual(DummyColor.Face_C_Pixel_AB, getColor(DummyFace.Face_C, 1, 0));
            Assert.AreEqual(DummyColor.Face_C_Pixel_BA, getColor(DummyFace.Face_C, 0, 1));
            Assert.AreEqual(DummyColor.Face_C_Pixel_BB, getColor(DummyFace.Face_C, 1, 1));
        }
    }
}
