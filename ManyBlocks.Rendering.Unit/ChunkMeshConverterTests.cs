﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Rhino.Mocks;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ManyBlocks.Rendering.Unit
{
    [TestClass]
    public sealed class ChunkMeshConverterTests
    {
        //TODO: texture coordinates are currently unimplemented and thus not included in the assertions in these tests.

        public enum DummyFace
        {
            DummyFaceA,
            DummyFaceB,
            DummyFaceC,
            DummyFaceD,
            DummyFaceE,
            DummyFaceF,
            DummyFaceG,
            DummyFaceH,
            DummyFaceI
        }

        public enum DummyColor
        {

        }

        [TestMethod]
        public void FacesGenerates()
        {
            var faceAtlas = MockRepository.GenerateStub<IFaceAtlas<DummyFace, DummyColor>>();
            faceAtlas.Stub(fa => fa.Size).Return(5);

            var atlas = new Dictionary<DummyFace, TextureCoordinate>
            {
                { DummyFace.DummyFaceA, new TextureCoordinate { X = 301, Y = 644 } },
                { DummyFace.DummyFaceB, new TextureCoordinate { X = 260, Y = 105 } },
                { DummyFace.DummyFaceC, new TextureCoordinate { X = 401, Y = 840 } },
                { DummyFace.DummyFaceD, new TextureCoordinate { X = 556, Y = 998 } },

                { DummyFace.DummyFaceE, new TextureCoordinate { X = 720, Y = 278 } },
                { DummyFace.DummyFaceF, new TextureCoordinate { X = 589, Y = 2021 } },
                { DummyFace.DummyFaceG, new TextureCoordinate { X = 2041, Y = 447 } },
                { DummyFace.DummyFaceH, new TextureCoordinate { X = 1056, Y = 881 } },

                { DummyFace.DummyFaceI, new TextureCoordinate { X = 1280, Y = 922 } }
            };

            faceAtlas.Stub(fa => fa.Faces).Return(new ReadOnlyDictionary<DummyFace, TextureCoordinate>(atlas));

            var chunkLocation = new ChunkLocation { X = 4, Y = -6, Z = 12 };
            var blockALocation = new LocalBlockLocation { X = 3, Y = 7, Z = 4 };
            var blockBLocation = new LocalBlockLocation { X = 6, Y = 19, Z = 30 };
            var blockCLocation = new LocalBlockLocation { X = 14, Y = 24, Z = 8 };
            var blockDLocation = new LocalBlockLocation { X = 17, Y = 2, Z = 26 };
            var blockELocation = new LocalBlockLocation { X = 20, Y = 29, Z = 10 };
            var blockFLocation = new LocalBlockLocation { X = 8, Y = 17, Z = 24 };
            var blockGLocation = new LocalBlockLocation { X = 26, Y = 6, Z = 11 };

            var blockFaceConverter = MockRepository.GenerateStub<IBlockFaceConverter<int, DummyFace>>();

            var chunk = new int[GlobalBlockLocation.ChunkSize, GlobalBlockLocation.ChunkSize, GlobalBlockLocation.ChunkSize];

            chunk[blockALocation.X, blockALocation.Y, blockALocation.Z] = 32;
            blockFaceConverter.Stub(bfc => bfc.Convert(
                Arg.Is(32),
                Arg.Is(new GlobalBlockLocation(chunkLocation, blockALocation)),
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceA).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy));

            chunk[blockBLocation.X, blockBLocation.Y, blockBLocation.Z] = 23;
            blockFaceConverter.Stub(bfc => bfc.Convert(
                Arg.Is(23),
                Arg.Is(new GlobalBlockLocation(chunkLocation, blockBLocation)),
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceB).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy));

            chunk[blockCLocation.X, blockCLocation.Y, blockCLocation.Z] = 48;
            blockFaceConverter.Stub(bfc => bfc.Convert(
                Arg.Is(48),
                Arg.Is(new GlobalBlockLocation(chunkLocation, blockCLocation)),
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceC).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy));

            chunk[blockDLocation.X, blockDLocation.Y, blockDLocation.Z] = 113;
            blockFaceConverter.Stub(bfc => bfc.Convert(
                Arg.Is(113),
                Arg.Is(new GlobalBlockLocation(chunkLocation, blockDLocation)),
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceD).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy));

            chunk[blockELocation.X, blockELocation.Y, blockELocation.Z] = 240;
            blockFaceConverter.Stub(bfc => bfc.Convert(
                Arg.Is(240),
                Arg.Is(new GlobalBlockLocation(chunkLocation, blockELocation)),
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceE).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy));

            chunk[blockFLocation.X, blockFLocation.Y, blockFLocation.Z] = 101;
            blockFaceConverter.Stub(bfc => bfc.Convert(
                Arg.Is(101),
                Arg.Is(new GlobalBlockLocation(chunkLocation, blockFLocation)),
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceF).Dummy));

            chunk[blockGLocation.X, blockGLocation.Y, blockGLocation.Z] = 141;
            blockFaceConverter.Stub(bfc => bfc.Convert(
                Arg.Is(141),
                Arg.Is(new GlobalBlockLocation(chunkLocation, blockGLocation)),
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceG).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceG).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceH).Dummy,
                out Arg<DummyFace?>.Out(DummyFace.DummyFaceI).Dummy));

            blockFaceConverter.Stub(bfc => bfc.Convert(
                Arg<int>.Is.Anything,
                Arg<GlobalBlockLocation>.Is.Anything,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy,
                out Arg<DummyFace?>.Out(null).Dummy));

            var chunkMeshConverter = new ChunkMeshConverter<int, DummyFace, DummyColor>
            {
                BlockFaceConverter = blockFaceConverter,
                FaceAtlas = faceAtlas
            };

            var expected = new[]
            {
                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 3, Y = 7, Z = 5 }),
                        TextureCoordinate = new TextureCoordinate { X = 301, Y = 649 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 3, Y = 8, Z = 5 }),
                        TextureCoordinate = new TextureCoordinate { X = 301, Y = 644 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 3, Y = 8, Z = 4 }),
                        TextureCoordinate = new TextureCoordinate { X = 306, Y = 644 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 3, Y = 7, Z = 4 }),
                        TextureCoordinate = new TextureCoordinate { X = 306, Y = 649 }
                    }
                },

                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 7, Y = 19, Z = 30 }),
                        TextureCoordinate = new TextureCoordinate { X = 260, Y = 110 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 7, Y = 20, Z = 30 }),
                        TextureCoordinate = new TextureCoordinate { X = 260, Y = 105 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 7, Y = 20, Z = 31 }),
                        TextureCoordinate = new TextureCoordinate { X = 265, Y = 105 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 7, Y = 19, Z = 31 }),
                        TextureCoordinate = new TextureCoordinate { X = 265, Y = 110 }
                    }
                },

                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 15, Y = 24, Z = 8 }),
                        TextureCoordinate = new TextureCoordinate { X = 401, Y = 845 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 15, Y = 24, Z = 9 }),
                        TextureCoordinate = new TextureCoordinate { X = 401, Y = 840 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 14, Y = 24, Z = 9 }),
                        TextureCoordinate = new TextureCoordinate { X = 406, Y = 840 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 14, Y = 24, Z = 8 }),
                        TextureCoordinate = new TextureCoordinate { X = 406, Y = 845 }
                    }
                },

                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 17, Y = 3, Z = 26 }),
                        TextureCoordinate = new TextureCoordinate { X = 556, Y = 1003 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 17, Y = 3, Z = 27 }),
                        TextureCoordinate = new TextureCoordinate { X = 556, Y = 998 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 18, Y = 3, Z = 27 }),
                        TextureCoordinate = new TextureCoordinate { X = 561, Y = 998 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 18, Y = 3, Z = 26 }),
                        TextureCoordinate = new TextureCoordinate { X = 561, Y = 1003 }
                    }
                },

                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 20, Y = 29, Z = 10 }),
                        TextureCoordinate = new TextureCoordinate { X = 720, Y = 283 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 20, Y = 30, Z = 10 }),
                        TextureCoordinate = new TextureCoordinate { X = 720, Y = 278 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 21, Y = 30, Z = 10 }),
                        TextureCoordinate = new TextureCoordinate { X = 725, Y = 278 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 21, Y = 29, Z = 10 }),
                        TextureCoordinate = new TextureCoordinate { X = 725, Y = 283 }
                    }
                },

                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 9, Y = 17, Z = 25 }),
                        TextureCoordinate = new TextureCoordinate { X = 589, Y = 2026 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 9, Y = 18, Z = 25 }),
                        TextureCoordinate = new TextureCoordinate { X = 589, Y = 2021 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 8, Y = 18, Z = 25 }),
                        TextureCoordinate = new TextureCoordinate { X = 594, Y = 2021 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 8, Y = 17, Z = 25 }),
                        TextureCoordinate = new TextureCoordinate { X = 594, Y = 2026 }
                    }
                },

                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 6, Z = 11 }),
                        TextureCoordinate = new TextureCoordinate { X = 2041, Y = 452 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 7, Z = 11 }),
                        TextureCoordinate = new TextureCoordinate { X = 2041, Y = 447 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 7, Z = 12 }),
                        TextureCoordinate = new TextureCoordinate { X = 2046, Y = 447 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 6, Z = 12 }),
                        TextureCoordinate = new TextureCoordinate { X = 2046, Y = 452 }
                    }
                },
                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 26, Y = 7, Z = 11 }),
                        TextureCoordinate = new TextureCoordinate { X = 2041, Y = 452 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 26, Y = 7, Z = 12 }),
                        TextureCoordinate = new TextureCoordinate { X = 2041, Y = 447 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 7, Z = 12 }),
                        TextureCoordinate = new TextureCoordinate { X = 2046, Y = 447 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 7, Z = 11 }),
                        TextureCoordinate = new TextureCoordinate { X = 2046, Y = 452 }
                    }
                },
                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 26, Y = 6, Z = 11 }),
                        TextureCoordinate = new TextureCoordinate { X = 1056, Y = 886 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 26, Y = 7, Z = 11 }),
                        TextureCoordinate = new TextureCoordinate { X = 1056, Y = 881 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 7, Z = 11 }),
                        TextureCoordinate = new TextureCoordinate { X = 1061, Y = 881 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 6, Z = 11 }),
                        TextureCoordinate = new TextureCoordinate { X = 1061, Y = 886 }
                    }
                },
                new Face
                {
                    A = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 6, Z = 12 }),
                        TextureCoordinate = new TextureCoordinate { X = 1280, Y = 927 }
                    },
                    B = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 27, Y = 7, Z = 12 }),
                        TextureCoordinate = new TextureCoordinate { X = 1280, Y = 922 }
                    },
                    C = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 26, Y = 7, Z = 12 }),
                        TextureCoordinate = new TextureCoordinate { X = 1285, Y = 922 }
                    },
                    D = new Vertex
                    {
                        Location = new GlobalBlockLocation(chunkLocation, new LocalBlockLocation { X = 26, Y = 6, Z = 12 }),
                        TextureCoordinate = new TextureCoordinate { X = 1285, Y = 927 }
                    }
                }
            };
            var actual = chunkMeshConverter.Faces(chunkLocation, chunk).ToArray();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [TestMethod]
        public void FacesEmpty()
        {
            var blockFaceConverter = MockRepository.GenerateStub<IBlockFaceConverter<int, DummyFace>>();

            var chunkLocation = new ChunkLocation { X = 4, Y = -6, Z = 12 };

            var chunk = new int[GlobalBlockLocation.ChunkSize, GlobalBlockLocation.ChunkSize, GlobalBlockLocation.ChunkSize];

            var chunkMeshConverter = new ChunkMeshConverter<int, DummyFace, DummyColor>
            {
                BlockFaceConverter = blockFaceConverter
            };

            Assert.IsFalse(chunkMeshConverter.Faces(chunkLocation, chunk).Any());
        }

        [TestMethod]
        public void Indices()
        {
            CollectionAssert.AreEqual(new ushort[] 
            {
                0, 1, 2, 2, 3, 0,
                4, 5, 6, 6, 7, 4,
                8, 9, 10, 10, 11, 8
            }, new ChunkMeshConverter<int, DummyFace, DummyColor>().Indices(3).ToArray());
        }

        [TestMethod]
        public void IndicesEmpty()
        {
            Assert.IsFalse(new ChunkMeshConverter<int, DummyFace, DummyColor>().Indices(0).Any());
        }
    }
}
