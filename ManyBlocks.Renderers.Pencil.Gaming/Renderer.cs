﻿using ManyBlocks.Rendering;
using Pencil.Gaming.Graphics;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Renderers.Pencil.Gaming
{
    /// <inheritdoc />
    [Export(typeof(IRenderer<,,>))]
    public sealed class Renderer<TBlock, TFace, TColor> : IRenderer<TBlock, TFace, TColor> where TBlock : struct where TFace : struct where TColor : struct
    {
        /// <inheritdoc />
        [Import]
        public IFaceAtlas<TFace, TColor> FaceAtlas;

        private float[] Matrix = new float[16];

        private void LoadMatrix(Matrix4x4 matrix)
        {
            Matrix[0] = matrix.M11;
            Matrix[1] = matrix.M12;
            Matrix[2] = matrix.M13;
            Matrix[3] = matrix.M14;

            Matrix[4] = matrix.M21;
            Matrix[5] = matrix.M22;
            Matrix[6] = matrix.M23;
            Matrix[7] = matrix.M24;

            Matrix[8] = matrix.M31;
            Matrix[9] = matrix.M32;
            Matrix[10] = matrix.M33;
            Matrix[11] = matrix.M34;

            Matrix[12] = matrix.M41;
            Matrix[13] = matrix.M42;
            Matrix[14] = matrix.M43;
            Matrix[15] = matrix.M44;

            GL.LoadMatrix(Matrix);
        }

        /// <inheritdoc />
        public void Render(int left, int bottom, int width, int height, Vector3 eye, Vector3 look, Vector3 up, float fieldOfView)
        {
            GL.Viewport(left, bottom, width, height);

            GL.Scissor(left, bottom, width, height);
            GL.ClearColor(1.0f, 0.0f, 0.1f, 1.0f);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Disable(EnableCap.ScissorTest);

            GL.MatrixMode(MatrixMode.Projection);
            LoadMatrix(Matrix4x4.CreatePerspectiveFieldOfView(1.0f, (float)width / (float)height, 1.0f, 1024.0f));

            // Reverse OpenGL's questionable at best Z+ being backwards.
            GL.MatrixMode(MatrixMode.Modelview);
            LoadMatrix(Matrix4x4.CreateLookAt(eye, eye + look, up) * Matrix4x4.CreateScale(new Vector3(1, 1, -1)));

            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.CullFace);
            GL.FrontFace(FrontFaceDirection.Cw);

            GL.Enable(EnableCap.Texture2D);
            GL.MatrixMode(MatrixMode.Texture);
            LoadMatrix(Matrix4x4.CreateScale(new Vector3(1.0f / FaceAtlas.Width, 1.0f / FaceAtlas.Height, 0.0f)));
            // TODO: Draw chunks here.
        }
    }
}
