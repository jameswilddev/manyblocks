﻿using ManyBlocks.Rendering;
using Pencil.Gaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Renderers.Pencil.Gaming
{
    /// <inheritdoc />
    [Export(typeof(IPlatform))]
    public sealed class Platform : IPlatform, IPartImportsSatisfiedNotification, IDisposable
    {
        /// <inheritdoc />
        public void Dispose()
        {
            Glfw.Terminate();
        }

        /// <inheritdoc />
        [Import]
        public IGame Game;

        /// <inheritdoc />
        public void OnImportsSatisfied()
        {
            if (!Glfw.Init()) throw new ExternalException("GLFW init failed");
            if (Glfw.OpenWindow(320, 240, 8, 8, 8, 0, 24, 0, WindowMode.Window) == 0)
                throw new ExternalException("Opening a window failed");
            Glfw.SetWindowTitle("ManyBlocks");
            Glfw.SetWindowSizeCallback((width, height) =>
            {
                Game.Render(width, height);
                Glfw.SwapBuffers();
            });
            Glfw.SwapInterval(true);
            while(Glfw.GetWindowParam(WindowParam.Opened) != 0)
            {
                int width, height;
                Glfw.GetWindowSize(out width, out height);
                Game.Render(width, height);
                Glfw.SwapBuffers();
            }
        }
    }
}
