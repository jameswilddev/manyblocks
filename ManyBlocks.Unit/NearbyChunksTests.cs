﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;

namespace ManyBlocks.Unit
{
    [TestClass]
    public sealed class NearbyChunksTests
    {
        private IEnumerable<ChunkLocation> Found;

        private PointOfView PointOfView = new PointOfView
        {
            Location = new Vector3
            {
                X = 80,
                Y = 240,
                Z = 306
            }
        };

        [TestInitialize]
        public void Initialize()
        {
            Found = new NearbyChunks
            {
                ServerSettings = new ServerSettings
                {
                    VisibleChunks = 45
                }
            }.NearTo(PointOfView);
        }

        [TestMethod]
        public void ReturnsNumberOfChunksRequested()
        {
            Assert.AreEqual(45, Found.Count());
        }

        [TestMethod]
        public void ReturnsContainingChunk()
        {
            CollectionAssert.Contains(Found.ToArray(), new GlobalBlockLocation(PointOfView.Location).Chunk);
        }

        [TestMethod]
        public void NoDuplicates()
        {
            CollectionAssert.AllItemsAreUnique(Found.ToArray());
        }

        private static IEnumerable<ChunkLocation> GetNeighbours(ChunkLocation of)
        {
            yield return new ChunkLocation { X = (short)(of.X - 1), Y = of.Y, Z = of.Z };
            yield return new ChunkLocation { X = (short)(of.X + 1), Y = of.Y, Z = of.Z };
            yield return new ChunkLocation { X = of.X, Y = (short)(of.Y - 1), Z = of.Z };
            yield return new ChunkLocation { X = of.X, Y = (short)(of.Y + 1), Z = of.Z };
            yield return new ChunkLocation { X = of.X, Y = of.Y, Z = (short)(of.Z - 1) };
            yield return new ChunkLocation { X = of.X, Y = of.Y, Z = (short)(of.Z + 1) };
        }

        [TestMethod]
        public void AllChunksAreNeighbours()
        {
            var unmatched = new HashSet<ChunkLocation>(Found);
            var matched = new HashSet<ChunkLocation>();

            matched.Add(unmatched.First());
            unmatched.Remove(matched.Single());

            var found = true;
            while(found)
            {
                found = false;
                foreach(var unmatch in unmatched)
                    if(matched.SelectMany(GetNeighbours).Contains(unmatch))
                    {
                        matched.Add(unmatch);
                        unmatched.Remove(unmatch);
                        found = true;
                        break;
                    }
            }

            Assert.IsFalse(unmatched.Any());
        }

        [TestMethod]
        public void NoBubbles()
        {
            // Each chunk neighbouring a selected chunk must have at least one direct line of sight to the bounds of the selected blocks.
            // This will fail if there are "bubbles" of unselected chunks.
            var neighbours = Found.SelectMany(GetNeighbours).Where(n => !Found.Contains(n)).ToList();

            foreach(var neighbour in neighbours)
            {
                if (!Found.Any(f => f.X > neighbour.X && f.Y == neighbour.Y && f.Z == neighbour.Z))
                    continue;

                if (!Found.Any(f => f.X < neighbour.X && f.Y == neighbour.Y && f.Z == neighbour.Z))
                    continue;

                if (!Found.Any(f => f.Y > neighbour.Y && f.X == neighbour.X && f.Z == neighbour.Z))
                    continue;

                if (!Found.Any(f => f.Y < neighbour.Y && f.X == neighbour.X && f.Z == neighbour.Z))
                    continue;

                if (!Found.Any(f => f.Z > neighbour.Z && f.Y == neighbour.Y && f.X == neighbour.X))
                    continue;

                if (!Found.Any(f => f.Z < neighbour.Z && f.Y == neighbour.Y && f.X == neighbour.X))
                    continue;

                Assert.Fail();
            }
        }
    }
}
