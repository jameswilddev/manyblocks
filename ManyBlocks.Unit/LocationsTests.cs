﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace ManyBlocks.Unit
{
    [TestClass]
    public sealed class LocationsTests
    {
        [TestMethod]
        public void ChunkBlockConstructor()
        {
            var chunk = new ChunkLocation { X = 24, Y = -18, Z = -3 };
            var block = new LocalBlockLocation { X = 16, Y = 22, Z = 14 };

            var result = new GlobalBlockLocation(chunk, block);

            Assert.AreEqual(784, result.X);
            Assert.AreEqual(-554, result.Y);
            Assert.AreEqual(-82, result.Z);
        }

        #region Chunk
        [TestMethod]
        public void ChunkXReturnsZeroAtZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { X = 0 }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsZeroUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize - 1 }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsOnePastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsOneUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize * 2 - 1 }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsTwoPastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(2, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize * 2 }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsNegativeOneAtNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation { X = -1 }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsNegativeOneUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation { X = -GlobalBlockLocation.ChunkSize }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsNegativeTwoPastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation { X = -GlobalBlockLocation.ChunkSize - 1 }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsNegativeTwoUntilTheEndOfTheSecondChunk()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation { X = -GlobalBlockLocation.ChunkSize - GlobalBlockLocation.ChunkSize }.Chunk.X);
        }

        [TestMethod]
        public void ChunkXReturnsNegativeThreePastTheEndOfTheSecondChunk()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation { X = -GlobalBlockLocation.ChunkSize - GlobalBlockLocation.ChunkSize - 1 }.Chunk.X);
        }

        [TestMethod]
        public void ChunkYReturnsZeroAtZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Y = 0 }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsZeroUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize - 1 }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsOnePastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsOneUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize * 2 - 1 }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsTwoPastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(2, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize * 2 }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsNegativeOneAtNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation { Y = -1 }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsNegativeOneUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation { Y = -GlobalBlockLocation.ChunkSize }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsNegativeTwoPastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation { Y = -GlobalBlockLocation.ChunkSize - 1 }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsNegativeTwoUntilTheEndOfTheSecondChunk()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation { Y = -GlobalBlockLocation.ChunkSize - GlobalBlockLocation.ChunkSize }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkYReturnsNegativeThreePastTheEndOfTheSecondChunk()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation { Y = -GlobalBlockLocation.ChunkSize - GlobalBlockLocation.ChunkSize - 1 }.Chunk.Y);
        }

        [TestMethod]
        public void ChunkZReturnsZeroAtZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Z = 0 }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsZeroUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize - 1 }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsOnePastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsOneUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize * 2 - 1 }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsTwoPastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(2, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize * 2 }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsNegativeOneAtNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation { Z = -1 }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsNegativeOneUntilTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation { Z = -GlobalBlockLocation.ChunkSize }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsNegativeTwoPastTheEndOfTheFirstChunk()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation { Z = -GlobalBlockLocation.ChunkSize - 1 }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsNegativeTwoUntilTheEndOfTheSecondChunk()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation { Z = -GlobalBlockLocation.ChunkSize - GlobalBlockLocation.ChunkSize }.Chunk.Z);
        }

        [TestMethod]
        public void ChunkZReturnsNegativeThreePastTheEndOfTheSecondChunk()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation { Z = -GlobalBlockLocation.ChunkSize - GlobalBlockLocation.ChunkSize - 1 }.Chunk.Z);
        }
        #endregion

        #region Block
        [TestMethod]
        public void BlockXReturnsZeroAtZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { X = 0 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneAtOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { X = 1 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneLessThanChunkSizeAtOneLessThanChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize - 1 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsZeroAtChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneAtOneGreaterThanChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize + 1 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneLessThanChunkSizeAtOneLessThanTwiceChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize * 2 - 1 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsZeroAtTwiceChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize * 2 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneLessThanChunkSizeAtNegativeOne()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { X = -1 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneAtOneGreaterThanNegativeChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { X = 1 - GlobalBlockLocation.ChunkSize }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsZeroAtNegativeChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { X = -GlobalBlockLocation.ChunkSize }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneLessThanChunkSizeAtOneLessThanNegativeChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { X = -1 - GlobalBlockLocation.ChunkSize }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneAtOneGreaterThanNegativeTwiceChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { X = 1 - GlobalBlockLocation.ChunkSize * 2 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsZeroAtNegativeTwiceChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { X = GlobalBlockLocation.ChunkSize * -2 }.Block.X);
        }

        [TestMethod]
        public void BlockXReturnsOneLessThanChunkSizeAtOneLessThanNegativeTwiceChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { X = -1 - GlobalBlockLocation.ChunkSize * 2 }.Block.X);
        }

        [TestMethod]
        public void BlockYReturnsZeroAtZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Y = 0 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneAtOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Y = 1 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneLessThanChunkSizeAtOneLessThanChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize - 1 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsZeroAtChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneAtOneGreaterThanChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize + 1 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneLessThanChunkSizeAtOneLessThanTwiceChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize * 2 - 1 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsZeroAtTwiceChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize * 2 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneLessThanChunkSizeAtNegativeOne()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Y = -1 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneAtOneGreaterThanNegativeChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Y = 1 - GlobalBlockLocation.ChunkSize }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsZeroAtNegativeChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Y = -GlobalBlockLocation.ChunkSize }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneLessThanChunkSizeAtOneLessThanNegativeChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Y = -1 - GlobalBlockLocation.ChunkSize }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneAtOneGreaterThanNegativeTwiceChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Y = 1 - GlobalBlockLocation.ChunkSize * 2 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsZeroAtNegativeTwiceChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Y = GlobalBlockLocation.ChunkSize * -2 }.Block.Y);
        }

        [TestMethod]
        public void BlockYReturnsOneLessThanChunkSizeAtOneLessThanNegativeTwiceChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Y = -1 - GlobalBlockLocation.ChunkSize * 2 }.Block.Y);
        }

        [TestMethod]
        public void BlockZReturnsZeroAtZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Z = 0 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneAtOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Z = 1 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneLessThanChunkSizeAtOneLessThanChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize - 1 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsZeroAtChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneAtOneGreaterThanChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize + 1 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneLessThanChunkSizeAtOneLessThanTwiceChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize * 2 - 1 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsZeroAtTwiceChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize * 2 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneLessThanChunkSizeAtNegativeOne()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Z = -1 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneAtOneGreaterThanNegativeChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Z = 1 - GlobalBlockLocation.ChunkSize }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsZeroAtNegativeChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Z = -GlobalBlockLocation.ChunkSize }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneLessThanChunkSizeAtOneLessThanNegativeChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Z = -1 - GlobalBlockLocation.ChunkSize }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneAtOneGreaterThanNegativeTwiceChunkSize()
        {
            Assert.AreEqual(1, new GlobalBlockLocation { Z = 1 - GlobalBlockLocation.ChunkSize * 2 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsZeroAtNegativeTwiceChunkSize()
        {
            Assert.AreEqual(0, new GlobalBlockLocation { Z = GlobalBlockLocation.ChunkSize * -2 }.Block.Z);
        }

        [TestMethod]
        public void BlockZReturnsOneLessThanChunkSizeAtOneLessThanNegativeTwiceChunkSize()
        {
            Assert.AreEqual(GlobalBlockLocation.ChunkSize - 1, new GlobalBlockLocation { Z = -1 - GlobalBlockLocation.ChunkSize * 2 }.Block.Z);
        }
        #endregion

        [TestMethod]
        public void AboveReturnsBlockAbove()
        {
            var result = new GlobalBlockLocation { X = 784, Y = -554, Z = -82 }.Above;

            Assert.AreEqual(784, result.X);
            Assert.AreEqual(-553, result.Y);
            Assert.AreEqual(-82, result.Z);
        }

        [TestMethod]
        public void BelowReturnsBlockBelow()
        {
            var result = new GlobalBlockLocation { X = 784, Y = -554, Z = -82 }.Below;

            Assert.AreEqual(784, result.X);
            Assert.AreEqual(-555, result.Y);
            Assert.AreEqual(-82, result.Z);
        }

        #region Vector3 to GlobalBlockLocation conversion

        [TestMethod]
        public void XBetweenZeroAndOneHalfReturnsZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation(new Vector3 { X = 0.2f }).X);
        }

        [TestMethod]
        public void XBetweenOneHalfAndOneReturnsZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation(new Vector3 { X = 0.8f }).X);
        }

        [TestMethod]
        public void XBetweenOneAndOneAndAHalfReturnsOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation(new Vector3 { X = 1.2f }).X);
        }

        [TestMethod]
        public void XBetweenOneAndAHalfAndTwoReturnsOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation(new Vector3 { X = 1.7f }).X);
        }

        [TestMethod]
        public void XBetweenTwoAndTwoAndAHalfReturnsTwo()
        {
            Assert.AreEqual(2, new GlobalBlockLocation(new Vector3 { X = 2.2f }).X);
        }

        [TestMethod]
        public void XBetweenTwoAndAHalfAndThreeReturnsTwo()
        {
            Assert.AreEqual(2, new GlobalBlockLocation(new Vector3 { X = 2.7f }).X);
        }


        [TestMethod]
        public void XBetweenZeroAndNegativeOneHalfReturnsNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation(new Vector3 { X = -0.2f }).X);
        }

        [TestMethod]
        public void XBetweenNegativeOneHalfAndNegativeOneReturnsNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation(new Vector3 { X = -0.8f }).X);
        }

        [TestMethod]
        public void XBetweenNegativeOneAndNegativeOneAndAHalfReturnsNegativeOne()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation(new Vector3 { X = -1.2f }).X);
        }

        [TestMethod]
        public void XBetweenNegativeOneAndAHalfAndNegativeTwoReturnsNegativeOne()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation(new Vector3 { X = -1.7f }).X);
        }

        [TestMethod]
        public void XBetweenNegativeTwoAndNegativeTwoAndAHalfReturnsNegativeTwo()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation(new Vector3 { X = -2.2f }).X);
        }

        [TestMethod]
        public void XBetweenNegativeTwoAndAHalfAndNegativeThreeReturnsNegativeTwo()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation(new Vector3 { X = -2.7f }).X);
        }


        [TestMethod]
        public void YBetweenZeroAndOneHalfReturnsZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation(new Vector3 { Y = 0.2f }).Y);
        }

        [TestMethod]
        public void YBetweenOneHalfAndOneReturnsZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation(new Vector3 { Y = 0.8f }).Y);
        }

        [TestMethod]
        public void YBetweenOneAndOneAndAHalfReturnsOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation(new Vector3 { Y = 1.2f }).Y);
        }

        [TestMethod]
        public void YBetweenOneAndAHalfAndTwoReturnsOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation(new Vector3 { Y = 1.7f }).Y);
        }

        [TestMethod]
        public void YBetweenTwoAndTwoAndAHalfReturnsTwo()
        {
            Assert.AreEqual(2, new GlobalBlockLocation(new Vector3 { Y = 2.2f }).Y);
        }

        [TestMethod]
        public void YBetweenTwoAndAHalfAndThreeReturnsTwo()
        {
            Assert.AreEqual(2, new GlobalBlockLocation(new Vector3 { Y = 2.7f }).Y);
        }


        [TestMethod]
        public void YBetweenZeroAndNegativeOneHalfReturnsNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation(new Vector3 { Y = -0.2f }).Y);
        }

        [TestMethod]
        public void YBetweenNegativeOneHalfAndNegativeOneReturnsNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation(new Vector3 { Y = -0.8f }).Y);
        }

        [TestMethod]
        public void YBetweenNegativeOneAndNegativeOneAndAHalfReturnsNegativeOne()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation(new Vector3 { Y = -1.2f }).Y);
        }

        [TestMethod]
        public void YBetweenNegativeOneAndAHalfAndNegativeTwoReturnsNegativeOne()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation(new Vector3 { Y = -1.7f }).Y);
        }

        [TestMethod]
        public void YBetweenNegativeTwoAndNegativeTwoAndAHalfReturnsNegativeTwo()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation(new Vector3 { Y = -2.2f }).Y);
        }

        [TestMethod]
        public void YBetweenNegativeTwoAndAHalfAndNegativeThreeReturnsNegativeTwo()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation(new Vector3 { Y = -2.7f }).Y);
        }


        [TestMethod]
        public void ZBetweenZeroAndOneHalfReturnsZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation(new Vector3 { Z = 0.2f }).Z);
        }

        [TestMethod]
        public void ZBetweenOneHalfAndOneReturnsZero()
        {
            Assert.AreEqual(0, new GlobalBlockLocation(new Vector3 { Z = 0.8f }).Z);
        }

        [TestMethod]
        public void ZBetweenOneAndOneAndAHalfReturnsOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation(new Vector3 { Z = 1.2f }).Z);
        }

        [TestMethod]
        public void ZBetweenOneAndAHalfAndTwoReturnsOne()
        {
            Assert.AreEqual(1, new GlobalBlockLocation(new Vector3 { Z = 1.7f }).Z);
        }

        [TestMethod]
        public void ZBetweenTwoAndTwoAndAHalfReturnsTwo()
        {
            Assert.AreEqual(2, new GlobalBlockLocation(new Vector3 { Z = 2.2f }).Z);
        }

        [TestMethod]
        public void ZBetweenTwoAndAHalfAndThreeReturnsTwo()
        {
            Assert.AreEqual(2, new GlobalBlockLocation(new Vector3 { Z = 2.7f }).Z);
        }


        [TestMethod]
        public void ZBetweenZeroAndNegativeOneHalfReturnsNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation(new Vector3 { Z = -0.2f }).Z);
        }

        [TestMethod]
        public void ZBetweenNegativeOneHalfAndNegativeOneReturnsNegativeOne()
        {
            Assert.AreEqual(-1, new GlobalBlockLocation(new Vector3 { Z = -0.8f }).Z);
        }

        [TestMethod]
        public void ZBetweenNegativeOneAndNegativeOneAndAHalfReturnsNegativeOne()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation(new Vector3 { Z = -1.2f }).Z);
        }

        [TestMethod]
        public void ZBetweenNegativeOneAndAHalfAndNegativeTwoReturnsNegativeOne()
        {
            Assert.AreEqual(-2, new GlobalBlockLocation(new Vector3 { Z = -1.7f }).Z);
        }

        [TestMethod]
        public void ZBetweenNegativeTwoAndNegativeTwoAndAHalfReturnsNegativeTwo()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation(new Vector3 { Z = -2.2f }).Z);
        }

        [TestMethod]
        public void ZBetweenNegativeTwoAndAHalfAndNegativeThreeReturnsNegativeTwo()
        {
            Assert.AreEqual(-3, new GlobalBlockLocation(new Vector3 { Z = -2.7f }).Z);
        }

        #endregion
    }
}
