﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Unit
{
    [TestClass]
    public sealed class EventSinkTests
    {
        public interface IHandler
        {
            void Callback(int metadata);
        }

        [TestMethod]
        public void RaiseNoSubscribersDoesNothing()
        {
            new EventSink<int>().Raise(556);
        }

        [TestMethod]
        public void RaiseExecutesAllSubscribersWithMetadata()
        {
            var mocks = new MockRepository();

            var callbackA = mocks.StrictMock<IHandler>();
            var callbackB = mocks.StrictMock<IHandler>();
            var callbackC = mocks.StrictMock<IHandler>();

            var eventSink = new EventSink<int>();

            using (mocks.Record())
            using (mocks.Ordered())
            {
                using (mocks.Unordered())
                {
                    callbackA.Callback(447);
                    callbackB.Callback(447);
                }

                using (mocks.Unordered())
                {
                    callbackA.Callback(224);
                    callbackB.Callback(224);
                    callbackC.Callback(224);
                }

                using (mocks.Unordered())
                {
                    callbackB.Callback(336);
                    callbackC.Callback(336);
                }
            }

            using (mocks.Playback())
            {
                eventSink.Event += callbackA.Callback;
                eventSink.Event += callbackB.Callback;
                eventSink.Raise(447);

                eventSink.Event += callbackC.Callback;
                eventSink.Raise(224);

                eventSink.Event -= callbackA.Callback;
                eventSink.Raise(336);
            }
        }
    }
}
