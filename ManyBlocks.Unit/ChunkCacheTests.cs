﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace ManyBlocks.Unit
{
    [TestClass]
    public sealed class ChunkCacheTests
    {
        #region Profiling
        [TestMethod]
        public async Task StatisticsDescribesStatus()
        {
            var mocks = new MockRepository();

            var chunkFactory = MockRepository.GenerateStub<IChunkFactory<int>>();
            var chunkChange = MockRepository.GenerateStub<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var redHerringC = new ChunkLocation { X = 35, Y = -6, Z = -7 };

            var task = new Task<int[,,]>(() => { throw new Exception(); });
            chunkFactory.Stub(cf => cf.Get(Arg.Is(redHerringC))).Return(task);
            chunkFactory.Stub(cf => cf.Get(Arg<ChunkLocation>.Is.Anything)).Return(Task.FromResult<int[,,]>(null));

            using (mocks.Record())
            using (mocks.Ordered())
            {
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                await chunkCache.Get(redHerringB);
                var notAwaited = chunkCache.Get(redHerringC);

                CollectionAssert.Contains(chunkCache.Statistics.ToArray(), new KeyValuePair<string, string>("Chunks in memory", "2"));
                CollectionAssert.Contains(chunkCache.Statistics.ToArray(), new KeyValuePair<string, string>("Chunks loading", "1"));
            }
        }
        #endregion

        #region TryGet
        [TestMethod]
        public async Task TryGetReturnsNullWhenChunkNotRequested()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                await chunkCache.Get(redHerringB);

                Assert.IsNull(chunkCache.TryGet(location));
            }
        }

        [TestMethod]
        public async Task TryGetReturnsNullWhenChunkNotFinishedLoading()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var gotTask = false;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });

                var task = new Task<int[,,]>(() => { throw new Exception(); });
                chunkFactory.Expect(cf => cf.Get(location)).Return(task).WhenCalled(mi => gotTask = true).Repeat.Once();

                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                var notAwaited = chunkCache.Get(location);
                await chunkCache.Get(redHerringB);

                // Not ideal, but ensures it started loading the chunk.
                var attempts = 0;

                while (!gotTask)
                {
                    Assert.IsFalse(attempts++ == 100);
                    await Task.Delay(100);
                }

                Assert.IsNull(chunkCache.TryGet(location));
            }
        }

        [TestMethod]
        public async Task TryGetReturnsChunkWhenFinishedLoading()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var chunk = new int[0, 0, 0];

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                chunkFactory.Expect(cf => cf.Get(location)).Return(Task.FromResult(chunk)).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = location });
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                await chunkCache.Get(location);
                await chunkCache.Get(redHerringB);

                Assert.AreSame(chunk, chunkCache.TryGet(location));
            }
        }

        [TestMethod]
        public async Task TryGetReturnsNullWhenExceptionThrownDirectly()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                chunkFactory.Expect(cf => cf.Get(location)).Throw(new Exception()).Repeat.Once();
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                try { await chunkCache.Get(location); } catch { }
                await chunkCache.Get(redHerringB);

                Assert.IsNull(chunkCache.TryGet(location));
            }
        }

        [TestMethod]
        public async Task TryGetReturnsNullWhenExceptionThrownByTask()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                var task = new Task<int[,,]>(() => { throw new Exception(); });
                chunkFactory.Expect(cf => cf.Get(location)).Return(task).WhenCalled(mi => task.Start()).Repeat.Once();
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                try { await chunkCache.Get(location); } catch { }
                await chunkCache.Get(redHerringB);

                Assert.IsNull(chunkCache.TryGet(location));
            }
        }
        #endregion

        #region Get
        [TestMethod]
        public async Task GetReturnsGeneratedChunk()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var chunk = new int[0, 0, 0];

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
                chunkFactory.Expect(cf => cf.Get(location)).Return(Task.FromResult(chunk)).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = location });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                await chunkCache.Get(redHerringB);
                Assert.AreSame(chunk, await chunkCache.Get(location));
            }
        }

        [TestMethod]
        public async Task RepeatedGetReturnsSameGeneratedChunk()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var chunk = new int[0, 0, 0];

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                chunkFactory.Expect(cf => cf.Get(location)).Return(Task.FromResult(chunk)).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = location });
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                Assert.AreSame(chunk, await chunkCache.Get(location));
                await chunkCache.Get(redHerringB);
                Assert.AreSame(chunk, await chunkCache.Get(location));
            }
        }

        [TestMethod]
        public async Task RepeatedGetWhileStillLoadingReturnsSameGeneratedChunk()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var chunk = new int[0, 0, 0];

            var task = new Task<int[,,]>(() => chunk);

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                chunkFactory.Expect(cf => cf.Get(location)).Return(task).Repeat.Once();
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
                chunkChange.Raise(new ChunkChange { Location = location });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                var resultA = chunkCache.Get(location);
                await chunkCache.Get(redHerringB);
                var resultB = chunkCache.Get(location);

                task.Start();
                Assert.AreSame(chunk, await resultA);
                Assert.AreSame(chunk, await resultB);
            }
        }

        [TestMethod]
        public async Task ExceptionThrownDirectlyBubbledUp()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var exception = new Exception();

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                chunkFactory.Expect(cf => cf.Get(location)).Throw(exception).Repeat.Once();
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                try
                {
                    await chunkCache.Get(location);
                    Assert.Fail();
                }
                catch(Exception ex)
                {
                    Assert.AreSame(exception, ex);
                }
                await chunkCache.Get(redHerringB);
                try
                {
                    await chunkCache.Get(location);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.AreSame(exception, ex);
                }
            }
        }

        [TestMethod]
        public async Task ExceptionThrownByTaskBubbledUp()
        {
            var mocks = new MockRepository();

            var chunkFactory = mocks.StrictMock<IChunkFactory<int>>();
            var chunkChange = mocks.StrictMock<IEventSink<ChunkChange>>();
            var chunkCache = new ChunkCache<int>
            {
                ChunkFactory = chunkFactory,
                ChunkChange = chunkChange
            };

            var redHerringA = new ChunkLocation { X = -4, Y = 7, Z = 30 };
            var redHerringB = new ChunkLocation { X = 12, Y = 8, Z = 31 };
            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var exception = new Exception();
            var task = new Task<int[,,]>(() => { throw exception; });

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkFactory.Expect(cf => cf.Get(redHerringA)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringA });
                chunkFactory.Expect(cf => cf.Get(location)).Return(task).WhenCalled(mi => task.Start()).Repeat.Once();
                chunkFactory.Expect(cf => cf.Get(redHerringB)).Return(Task.FromResult(new int[0, 0, 0])).Repeat.Once();
                chunkChange.Raise(new ChunkChange { Location = redHerringB });
            }

            using (mocks.Playback())
            {
                await chunkCache.Get(redHerringA);
                try
                {
                    await chunkCache.Get(location);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.AreSame(exception, ex);
                }
                await chunkCache.Get(redHerringB);
                try
                {
                    await chunkCache.Get(location);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.AreSame(exception, ex);
                }
            }
        }
        #endregion
    }
}
