﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyBlocks.Unit
{
    [TestClass]
    public sealed class BlockCacheTests
    {
        [TestMethod]
        public void TryGetReturnsNullWhenChunkNotCached()
        {
            var mocks = new MockRepository();

            var chunkCache = mocks.StrictMock<IChunkCache<int>>();
            var blockCache = new BlockCache<int>
            {
                ChunkCache = chunkCache
            };

            var location = new GlobalBlockLocation { X = 784, Y = -554, Z = -82 };

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkCache.Expect(cc => cc.TryGet(new ChunkLocation { X = 24, Y = -18, Z = -3 })).Return(null);
            }

            using (mocks.Playback())
            {
                Assert.IsNull(blockCache.TryGet(location));
            }
        }

        [TestMethod]
        public void TryGetReturnsValueWhenChunkCached()
        {
            var mocks = new MockRepository();

            var chunkCache = mocks.StrictMock<IChunkCache<int>>();
            var blockCache = new BlockCache<int>
            {
                ChunkCache = chunkCache
            };

            var location = new GlobalBlockLocation { X = 784, Y = -554, Z = -82 };
            var chunk = new int[GlobalBlockLocation.ChunkSize, GlobalBlockLocation.ChunkSize, GlobalBlockLocation.ChunkSize];
            chunk[16, 22, 14] = 4487;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkCache.Expect(cc => cc.TryGet(new ChunkLocation { X = 24, Y = -18, Z = -3 })).Return(chunk);
            }

            using (mocks.Playback())
            {
                Assert.AreEqual(4487, blockCache.TryGet(location));
            }
        }

        [TestMethod]
        public async Task GetReturnsBlockFromChunk()
        {
            var mocks = new MockRepository();

            var chunkCache = mocks.StrictMock<IChunkCache<int>>();
            var blockCache = new BlockCache<int>
            {
                ChunkCache = chunkCache
            };

            var location = new GlobalBlockLocation { X = 784, Y = -554, Z = -82 };
            var chunk = new int[GlobalBlockLocation.ChunkSize, GlobalBlockLocation.ChunkSize, GlobalBlockLocation.ChunkSize];
            chunk[16, 22, 14] = 4487;

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkCache.Expect(cc => cc.Get(new ChunkLocation { X = 24, Y = -18, Z = -3 })).Return(Task.FromResult(chunk));
            }

            using (mocks.Playback())
            {
                Assert.AreEqual(4487, await blockCache.Get(location));
            }
        }
    }
}
