﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Rhino.Mocks;

namespace ManyBlocks.Unit
{
    [TestClass]
    public sealed class ChunkFactoryTests
    {
        [TestMethod]
        public async Task AlreadyExistsReturnsLoadedChunk()
        {
            var mocks = new MockRepository();

            var chunkGenerator = mocks.StrictMock<IChunkGenerator<int>>();
            var chunkPersistence = mocks.StrictMock<IChunkPersistence<int>>();

            var chunkFactory = new ChunkFactory<int>
            {
                ChunkGenerator = chunkGenerator,
                ChunkPersistence = chunkPersistence
            };

            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var chunk = new int[0, 0, 0];

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkPersistence.Expect(cp => cp.TryLoad(location)).Return(Task.FromResult(chunk)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                Assert.AreSame(chunk, await chunkFactory.Get(location));
            }
        }

        [TestMethod]
        public async Task DoesNotExistCreatesChunk()
        {
            var mocks = new MockRepository();

            var chunkGenerator = mocks.StrictMock<IChunkGenerator<int>>();
            var chunkPersistence = mocks.StrictMock<IChunkPersistence<int>>();

            var chunkFactory = new ChunkFactory<int>
            {
                ChunkGenerator = chunkGenerator,
                ChunkPersistence = chunkPersistence
            };

            var location = new ChunkLocation { X = 11, Y = -4, Z = -3 };

            var chunk = new int[0, 0, 0];

            using (mocks.Record())
            using (mocks.Ordered())
            {
                chunkPersistence.Expect(cp => cp.TryLoad(location)).Return(Task.FromResult<int[,,]>(null)).Repeat.Once();
                chunkGenerator.Expect(cg => cg.Generate(location)).Return(Task.FromResult(chunk)).Repeat.Once();
                chunkPersistence.Expect(cp => cp.Create(location, chunk)).Return(Task.FromResult(true)).Repeat.Once();
            }

            using (mocks.Playback())
            {
                Assert.AreSame(chunk, await chunkFactory.Get(location));
            }
        }
    }
}
